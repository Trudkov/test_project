
from sklearn.pipeline import Pipeline

from sklearn.preprocessing import StandardScaler

from sklearn.feature_selection import VarianceThreshold
from src.model.select_features import Calculate_Vif
import feature_engine.missing_data_imputers as mdi
from category_encoders import OrdinalEncoder

def make_pipeline_steps(model,vif_threshold = 5.0, variance_threshold = 0.05):

    steps = [

        ('num_imputer',mdi.MeanMedianImputer(imputation_method='median')),
        ('cat_imputer',  mdi.CategoricalVariableImputer(imputation_method='frequent')),
        ('oe', OrdinalEncoder()),
        ('scaler', StandardScaler()),
         ('vif',Calculate_Vif(thresh=vif_threshold)),
         ('backEl',VarianceThreshold(threshold=variance_threshold)),
         ('model',model)
        ]
    return Pipeline(steps)


