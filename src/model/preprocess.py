
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler

def encode_data(X):
    label_encoder = LabelEncoder()

    for i in range(X.shape[1]):
        # for encoding of all columns having unique values lower than 5
        if len(np.unique(X[:, i])) < 5:
            X[:, i] = label_encoder.fit_transform(X[:, i])

    return X





