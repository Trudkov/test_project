from statsmodels.stats.outliers_influence import variance_inflation_factor


from sklearn.base import TransformerMixin
from sklearn.base import BaseEstimator
import numpy as np


class Mean_Mode_Imputer(TransformerMixin, BaseEstimator):

    """Hand made imputer. Mean for numeric columns and mode for categorical columns"""
    def __init__(self, columns=None):
        self.columns = columns
        self.mapping = None

    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X, y, **fit_params).transform(X, y)

    def _get_columns(self, X):
        columns = self.columns
        if columns is None:
            columns = X.columns

        return columns

    def fit(self, X, y, **fit_params):


        modes = X.select_dtypes(exclude=np.number).mode().iloc[0]
        means = X.mean()
        both = modes.append(means)
        self.mapping = both


        return self

    def transform(self, X, y=None):
        X = X.copy(deep=True)

        X.fillna(self.mapping, inplace=True)


        return X




class Calculate_Vif(TransformerMixin, BaseEstimator):
    """Addopted  variance inflation factor
    """

    def __init__(self, thresh = 5.0, columns=None):
        self.thresh = thresh
        self.columns = columns


    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X, y, **fit_params).transform(X, y)

    def _get_columns(self, X):
        columns = self.columns
        if columns is None:
            columns = X.columns

        return columns

    def fit(self, X, y, **fit_params):

        self.variables = list(range(X.shape[1]))
        dropped = True
        while dropped:
            dropped = False
            vif = [variance_inflation_factor(X[:, self.variables], i) for i in range(X[:, self.variables].shape[1])]
            maxloc = vif.index(max(vif))


            if max(vif) > self.thresh:
                del self.variables[maxloc]
                dropped = True
        #print('Remaining variables:')
        #print(self.variables)
        return self


    def transform(self, X, y=None):

        X = X.copy()


        return X[:, self.variables]

