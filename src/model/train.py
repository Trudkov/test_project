
from sklearn.linear_model import LogisticRegression
import numpy as np
from src.data import obtain
from sklearn.model_selection import train_test_split
from src.model.pipeline import make_pipeline_steps
from sklearn.metrics import accuracy_score,precision_score,recall_score

from sklearn.model_selection import GridSearchCV, StratifiedKFold

from src.config import GRID_SEARCH_NJOBS, RANDOM_STATE
import pickle
import warnings
warnings.filterwarnings("ignore")
from src.data.vizualize import make_plots
import sys
import os
if not sys.warnoptions:
    warnings.simplefilter("ignore")
    os.environ["PYTHONWARNINGS"] = "ignore"

def model_evaluation(classifier, X, y):
    """Evaluation of ckassifier
    X - test dataset
    y - test metrics
    """
    y_pred = classifier.predict(X)
    accuracy = accuracy_score(y, y_pred)
    precision = precision_score(y, y_pred)
    recall = recall_score(y, y_pred)
    print("accuracy score:",accuracy)
    print("precision score:",precision )
    print("recall score",recall)

    return accuracy



def build_log(X,y,folds = 5):

    '''GridSearch on Logistic Regression
    X,y - train data ans labels,
    folds - CV folds
    '''

    log_params = {

        "model__C": np.logspace(-3,3,7),
        "model__penalty": ['l1','l2','elasticnet','none'],
        "model__solver" : ['newton-cg','saga','lbfgs','liblinear']

    }

    kfold = StratifiedKFold(n_splits=folds, random_state=RANDOM_STATE, shuffle=True)
    pipeline = make_pipeline_steps(LogisticRegression(random_state=RANDOM_STATE))
    gs = GridSearchCV(
        pipeline,
        param_grid=log_params,
        cv=kfold,
        n_jobs=GRID_SEARCH_NJOBS,
        scoring="accuracy",
        verbose=1,
        error_score=0
    )
    gs.fit(X,y)
    return  gs



def train():
    """Training process through obtaining, evaluation metrics and finding best parameters"""

    X,y = obtain.obtain_data(path ='../../data/raw/WA_Fn-UseC_-Telco-Customer-Churn.csv',
                             dtype={'TotalCharges':np.float32, 'MonthlyCharges': np.float32},
                             na_vals= [' ','','#NA','NA','NULL','NaN', 'nan', 'n/a'],
                      drop_columns = ['customerID', 'PaperlessBilling', 'PaymentMethod'])

    # make visualizations
    make_plots(X,y,save_path ='../../reports/figures')

    # Divide on train/test, estimate metrics
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)
    print('Estimate metrics on test data:')
    log = LogisticRegression(random_state=RANDOM_STATE)
    model = make_pipeline_steps(log)
    model.fit(X_train, y_train)
    model_evaluation(model, X_test,y_test)

    print()
    # Start Grid Search
    print('Finding best parameters for Logistic regression through GridSearchCV...')
    gs = build_log(X,y)
    print('Grid Search best results Logistic Regression:')
    print(gs.best_params_,gs.best_score_)

    #Save best output
    with open('../../models/log_reg.p', 'wb') as file:
        pickle.dump(gs.best_estimator_,file)

if __name__ == "__main__":
    train()


