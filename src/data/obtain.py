import pandas as pd
import numpy as np


def read_data(path, na_values, dtype):

    """Read csv data
    path - path to csv file
    na_values - examples of missing values
    dtype - data types for certain columns
    """

    dataset = pd.read_csv(path, na_values = na_values,
                      dtype = dtype )

    return dataset


def drop_useless(dataset,columns):
    """Drop useless cols
    columns  - list of columns
    """
    # test whether columns in initial data
    assert (set(columns) <= set(dataset.columns)) == True

    dataset.drop(columns=columns
                 , axis=1, inplace=True)
    # test whether dataframe is not empty
    assert dataset.empty == False

    return dataset


def obtain_data(path, na_vals, dtype, drop_columns ):

    """Obtain data fill missing vals and return X and y
    path - path to csv file
    columns - useless columns t drop
    dtype - data types for certain columns
    drop_columns  - list of useless columns
    """
    dataset = read_data(path,na_vals, dtype)
    dataset = drop_useless(dataset,drop_columns)


    X = dataset.drop('Churn',axis = 1)
    y = dataset.Churn.map({'Yes':1,
                           'No':0})
    # test renaming Y
    assert sum(np.isnan(y)) == 0

    return X,y


