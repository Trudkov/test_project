import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

def total_vs_month(churn0, churn1, save_path,title = 'Total Charges VS Monthly Charges'):
    print(churn1.head(2))
    plt.figure(figsize=(10, 5))
    plt.title(title)
    sns.scatterplot('TotalCharges', 'MonthlyCharges', data=churn1)
    sns.scatterplot('TotalCharges', 'MonthlyCharges', data=churn0)
    plt.savefig(save_path+title+'.png')
    plt.show()

def tenure_vs_total(churn0, churn1, save_path, title = 'Tenure VS Total Charges',):
    plt.figure(figsize=(10, 5))
    plt.title(title)
    sns.scatterplot('MonthlyCharges', 'tenure', data=churn1)
    sns.scatterplot('MonthlyCharges', 'tenure', data=churn0)
    plt.savefig(save_path+title+'.png')
    plt.show()

def tensure_vs_month(churn0, churn1, save_path, title='Tenure VS Monthly Charges'):
    plt.figure(figsize=(10, 5))
    plt.title(title)
    sns.scatterplot('MonthlyCharges', 'tenure', data=churn1)
    sns.scatterplot('MonthlyCharges', 'tenure', data=churn0)
    plt.savefig(save_path+title+'.png')
    plt.show()

def make_plots(X,y, save_path):
    dataset = pd.concat([X,y],axis = 1)

    churn1 = dataset[dataset['Churn'] == 1][:1000]
    churn0 = dataset[dataset['Churn'] == 0][:1000]

    total_vs_month(churn0, churn1, save_path,)
    tenure_vs_total(churn0, churn1, save_path,)
    tensure_vs_month(churn0, churn1, save_path,)
